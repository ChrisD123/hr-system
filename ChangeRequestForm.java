package hrsystem;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 * ChangeRequestForm Frame. Allows the user to send a Change Request to HR.
 * @author Chris
 */
public class ChangeRequestForm extends javax.swing.JFrame {

    public static int employeeID; // Employee ID of the user who is currently logged in
    UserDetails userDetails; // UserDetails frame variable, used to return to this frame
    
    /**
     * Creates new form ChangeRequestForm
     */
    public ChangeRequestForm() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtRequest = new javax.swing.JTextArea();
        btnSubmitRequest = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jLabel1.setText("Request Change");

        jLabel2.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel2.setText("Request");

        txtRequest.setColumns(20);
        txtRequest.setRows(5);
        jScrollPane1.setViewportView(txtRequest);

        btnSubmitRequest.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnSubmitRequest.setText("Submit Request");
        btnSubmitRequest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmitRequestActionPerformed(evt);
            }
        });

        btnBack.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel2)
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLabel1)))
                .addContainerGap(79, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(btnSubmitRequest)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnBack)
                .addGap(48, 48, 48))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(55, 55, 55)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSubmitRequest)
                    .addComponent(btnBack))
                .addGap(33, 33, 33))
        );

        pack();
    }// </editor-fold>                        

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {                                        
        // Destroys current frame and returns to UserDetails frame
        this.dispose();
        userDetails.employeeID = employeeID;
        userDetails.main(null);
    }                                       

    private void btnSubmitRequestActionPerformed(java.awt.event.ActionEvent evt) {                                                 
        // Saves request to database and returns to UserDetails form
        sendRequest();
    }                                                

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ChangeRequestForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ChangeRequestForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ChangeRequestForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ChangeRequestForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ChangeRequestForm().setVisible(true);
            }
        });
    }
    
    public void sendRequest()
    {
        // String variable to store request
        String request = txtRequest.getText();
        
        // SQL Queries
        String reqQueryID = "SELECT MAX(Request_ID) AS 'Highest_ID' FROM ChangeRequests"; // Gets the current highest Request_ID in ChangeRequests table
        String insertReqQuery = "INSERT INTO ChangeRequests VALUES(?,?,?,?,?,?,?)"; // Inserts new request into ChangeRequests table
        
        // Creates DateFormat object in order to store the current date into a String variable
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new java.util.Date();
        
        
        String dateCreated = dateFormat.format(date); // Stores current date
        
        int highestRequestID = 0; // int variable to store highest RequestID in table, will be set later
        
        // Connection and PreparedStatement variables
        Connection c = null;
        PreparedStatement getReqID = null;
        PreparedStatement insertReq = null;
        
        try
        {
            // Get connection
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:./database/hrsystem.db");
            
            // Execute query and return ResultSet object
            getReqID = c.prepareStatement(reqQueryID);
            ResultSet req = getReqID.executeQuery();
            
            if(req.next())
            {
                // Get the highest request ID, and add 1 to it
                highestRequestID = req.getInt("Highest_ID");
                highestRequestID += 1;
                
                // Set variables into query and execute
                insertReq = c.prepareStatement(insertReqQuery);
                insertReq.setInt(1, highestRequestID);
                insertReq.setInt(2, employeeID);
                insertReq.setString(3, request);
                insertReq.setInt(4, 0);
                insertReq.setInt(5, 0);
                insertReq.setString(6, dateCreated);
                insertReq.setInt(7, 0);
                
                insertReq.executeUpdate();
                
                // Display message to tell the user that their request has been received
                JOptionPane.showMessageDialog(this, "Your request has been received.");
                
                // Destroy current frame and return to UserDetails frame
                this.dispose();
                userDetails.employeeID = employeeID;
                userDetails.main(null);
            }
            
            req.close();
            insertReq.close();
            getReqID.close();
            c.close();
        }
        catch(Exception e)
        {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    }

    // Variables declaration - do not modify                     
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnSubmitRequest;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txtRequest;
    // End of variables declaration                   
}
