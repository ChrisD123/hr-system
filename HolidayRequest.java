package hrsystem;

/**
 * Class used to create and store HolidayRequest objects with data
 * obtained from the database.
 * @author Chris
 */
public class HolidayRequest {
    
    private int holidayRequestID; // ID of current Holiday Request
    private int employeeID; // ID of Employee who made the request
    private String startDate; // Start date of requested holiday
    private String endDate; // End date of requested holiday
    
    public HolidayRequest(int holidayRequestID, int employeeID, String startDate, String endDate)
    {
        this.holidayRequestID = holidayRequestID;
        this.employeeID = employeeID;
        this.startDate = startDate;
        this.endDate = endDate;
    }
    
    /**
     * getRequestID()
     * @return Returns holidayRequestID int variable
     */
    public int getRequestID()
    {
        return holidayRequestID;
    }
    
    /**
     * getEmployeeID()
     * @return Returns employeeID int variable
     */
    public int getEmployeeID()
    {
        return employeeID;
    }
    
    /**
     * getStartDate()
     * @return Returns startDate String variable
     */
    public String getStartDate()
    {
        return startDate;
    }
    
    /**
     * getEndDate()
     * @return returns endDate String variable
     */
    public String getEndDate()
    {
        return endDate;
    }
}
