package hrsystem;

/**
 * Class used to create and store ChangeRequest objects with data
 * obtained from the database.
 * @author Chris
 */
public class ChangeRequest {
    
    private int requestID; // ID of current Change Request
    private int employeeID; // ID of Employee who sent the request
    private String request; // The text of the request sent by the employee
    private String dateCreated; // Date that the request was sent
    
    public ChangeRequest(int requestID, int employeeID, String request, String dateCreated)
    {
        this.requestID = requestID;
        this.employeeID = employeeID;
        this.request = request;
        this.dateCreated = dateCreated;
    }
    
    /**
     * getRequestID()
     * @return Returns requestID int variable
     */
    public int getRequestID()
    {
        return requestID;
    }
    
    /**
     * getEmployeeID()
     * @return Returns employeeID int variable
     */
    public int getEmployeeID()
    {
        return employeeID;
    }
    
    /**
     * getRequest()
     * @return Returns request String variable
     */
    public String getRequest()
    {
        return request;
    }
    
    /**
     * getDateCreated()
     * @return Returns dateCreated String variable
     */
    public String getDateCreated()
    {
        return dateCreated;
    }
}
